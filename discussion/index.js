let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

num1 += num4;
console.log(num1);

num1 += num1;
console.log(num1);

num1 += 55;
console.log(num1);

let string1 = "Boston";
let	string2 = "Celtics";

string1 += string2;
console.log(string1);
console.log(string2);

// console.log(15 += num1); this produces error, do not use addition assignment operator when the left operand is just data.

console.log(num1 -= num2);
console.log(num1 -= num4);

  // NaN means not applicable because you use two different data types

num2 *= num3;
console.log(num2);

num4 /= num3;
console.log(num4);


// Mathematical Operations - follows MDAS

let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);

// Type Coercion - automatic or implicit conversion of values

// Mini Activity

let favFood = "spaghetti";
let sum = 150 + 9;
let product = 100 * 90;
let isActive2 = true;

let array = ["Jollibee", "Mcdo", "Kenny Rogers", "TGFridays", "Burger King"];

let artist = {
    firstName: "Lukas",
    lastName: "Forchhammer",
    stageName: "Lukas Graham",
    birthday: "September 18 1988",
    age: 33,
    bestAlbum: "Lukas Graham (2012 album)",
    bestSong: "7 Years" ,
    isActive: true,
}

console.log(favFood);
console.log(sum);
console.log(product);
console.log(isActive2);
console.log(array);
console.log(artist);

function findQuotient (numb1, numb2) {
  quotient = numb1/numb2;
  return quotient;
}

let quotient;
findQuotient(10,2);
console.log("The result of the division is: " + quotient);

// Mini Activity

let username;
let password;

function login(username, password){
  if(typeof username === "String" && typeof password === "String"){
    console.log("Both arguments are strings.");
  }
  else if (password.length < 8 && username.length >= 8) {
    console.log("Password is too short.");
  }
  else if (username.length < 8 && password.length >= 8) {
    console.log("Username is too short.");
  }
  else if(password.length >= 8 && username.length >= 8){
    console.log("Credentials are good.");
  }
  else {
    console.log("Credentials too short.");
}
}

login("Myu", "assssssss");


function shirtDay(day){
    if (day.toLowerCase() == "monday"){
      alert("Today is " + day, + "wear Black") }
    else if (day == "Tuesday"){
      alert("Today is " + day, + "wear Green") }
    else if (day == "Wednesday"){
      alert("Today is " + day, + "wear Yellow") }
    else if (day == "Thursday"){
      alert("Today is " + day, + "wear Red") }
      else if (day == "Friday"){
      alert("Today is " + day, + "wear Violet")}
    else if (day == "Saturday"){
      alert("Today is " + day, + "wear Blue")}
    else if (day == "Sunday"){
      alert("Today is " + day, + "wear White")}
    else {
        alert("invalid input. Enter a valid day of the week.")}

}
console.log(shirtDay("Monday"));

/*
 switch statement is alternative to an if, else-if, else tree. It is where the date being evaluated or check is an expected input.
 - to select one of many code blocks/statements to be execute
 - will compare your expression/condition to matkc with a case. If a case matches the given expression or condition, then statement for that case will run/execute.
 - the switch expression and your case is evaluated to match. If it doesn't match, we will check for the next caes, if no cases match the expression/condition, then the default code block will be executed. If we do have match, the statement for that case will run/execute.
 syntax: switch(expression/condition){
  case value:
      statement;
      break;
  case value:
      statement;
      break;
  default:
      statement;
      break;
 }
*/

let hero = "superman";

switch(hero){
  case "Jose Rizal":
    console.log("National Hero of the Philippines.");
    break;
  case "George Washington":
    console.log("Hero of the American Revolution.");
    break;
  case "Hercules":
    console.log("Legendary Hero of the Greek.");
    break;
  default:
    console.log("invalid input.");
}

let role = "Admin";

function roleChecker(role){
  switch(role){
    case "Admin":
      console.log("Welcome Admin to the dashboard.");
      break;
    case "User":
      console.log("You are not authorize to view this page.");
      break;
    case "Guest":
      console.log("Go to registration page to register.");
      break;            
  default:
    console.log("invalid role, please try again.");
    break;
  }
}

/*
  Ternary Operators - shorthand way of writing if-else statements
  syntax. condition ? if-statement: else-statement
*/

let price = 5000;

if(price > 1000){
  console.log("Price is over 1000");
}
else{
  console.log("Price is less than 1000");}

  price > 1000 ? console.log("Price is over 1000.") : console.log("Price is less than 1000");

  let hero2 = "Goku";

  hero2 === "Vegeta" ? console.log("You are the prince of all saiyans.") : console.log("You're not given a royalty");


